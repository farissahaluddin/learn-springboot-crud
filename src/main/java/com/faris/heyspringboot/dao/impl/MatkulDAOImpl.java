package com.faris.heyspringboot.dao.impl;

import com.faris.heyspringboot.dao.MatkulDAO;
import com.faris.heyspringboot.entity.Matkul;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;


import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class MatkulDAOImpl implements MatkulDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Matkul> findByNamaMatkul(Matkul param) {
        String sql = "select * from tbl_matkul where nama_matkul like ?";
        return jdbcTemplate.query(sql, new Object[]{"%" + param.getNama_matkul() + "%"}, new BeanPropertyRowMapper<>(Matkul.class));
    }

    @Override
    public Matkul save(Matkul param) {
        String sql = "insert into tbl_matkul (nama_matkul,sks) values (?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection ->{
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, param.getNama_matkul());
            ps.setInt(2, param.getSks());
            return ps;
        }, keyHolder);
        param.setId_matkul(keyHolder.getKey().intValue());
        return param;
    }

    @Override
    public Matkul update(Matkul param) {
        String sql = "update tbl_matkul set nama_matkul=?,sks=? where id_matkul=?";

        int mk = jdbcTemplate.update(connection ->{
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, param.getNama_matkul());
            ps.setInt(2, param.getSks());
            ps.setInt(3, param.getId_matkul());
            return ps;
        });
        param.setId_matkul(mk);
        return param;
    }

    @Override
    public int delete(Matkul param) {
        String sql = "delete from tbl_matkul where id_matkul=?";
        int mk = jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, param.getId_matkul());
            return ps;
        });
        return mk;
    }

    @Override
    public Matkul findById(int id) {
        String sql = "select * from tbl_matkul where id_matkul=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Matkul.class));
    }

    @Override
    public List<Matkul> findAll() {
        String sql = "select * from tbl_matkul";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Matkul.class));
    }

}
