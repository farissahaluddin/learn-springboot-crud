package com.faris.heyspringboot.dao;

import com.faris.heyspringboot.entity.Matkul;

import java.util.List;

public interface MatkulDAO extends BaseDAO<Matkul> {

    List<Matkul> findByNamaMatkul(Matkul param);
}
