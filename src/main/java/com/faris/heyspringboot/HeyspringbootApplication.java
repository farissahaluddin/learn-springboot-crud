package com.faris.heyspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeyspringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeyspringbootApplication.class, args);
    }

}
