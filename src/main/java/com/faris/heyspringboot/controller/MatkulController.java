package com.faris.heyspringboot.controller;

import com.faris.heyspringboot.entity.Matkul;
import com.faris.heyspringboot.service.MatkulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/matakuliah")
public class MatkulController {

    @Autowired
    private MatkulService matkulService;


    @GetMapping(path = "/create")
    public String viewCreate(Model model) {
        model.addAttribute("dataSets", new Matkul());
        return "/matakuliah/create";
    }

    @PostMapping(value = "/create")
    public String save(Matkul param) {
        Matkul data = matkulService.save(param);
        if (data.getId_matkul() == 0) {
            return "redirect:/matakuliah/create?failed";
        } else {
            return "redirect:/matakuliah?usuccess"; //matakuliah/create?
        }
    }


    @GetMapping(path = "/update/{id_matkul}")
    public String viewUpdate(Model model, @PathVariable(value = "id_matkul") int id_matkul) {
        model.addAttribute("dataSets", matkulService.findById(id_matkul));
        return "/matakuliah/update";
    }

    @PutMapping(path = "/update")
    public String update(Matkul param) {
        Matkul data = matkulService.update(param);
        if (data.getId_matkul() == 0) {
            return "redirect:/matakuliah?ufailed";
        } else {
            return "redirect:/matakuliah?usuccess";
        }
    }

    @DeleteMapping(path = "/delete")
    public String delete(Matkul param) {
        int data = matkulService.delete(param);
        if (data == 0) {
            return "redirect:/matakuliah?dfailed";
        } else {
            return "redirect:/matakuliah?dsuccess";
        }
    }

    @GetMapping(path = "")
    public String viewData(Model model, @RequestParam(value = "search", required = false) String param,
                           @RequestParam(value = "filter", required = false) String param1) {
        if (param == null && param1 == null) {
            model.addAttribute("dataSets", matkulService.findAll());
        } else {
            Matkul matkul = new Matkul();
            matkul.setNama_matkul(param);
            model.addAttribute("dataSets", matkulService.findByNamaMatkul(matkul));
        }
        return "/matakuliah/list";
    }

}
