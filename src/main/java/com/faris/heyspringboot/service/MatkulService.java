package com.faris.heyspringboot.service;

import com.faris.heyspringboot.entity.Matkul;

import java.util.List;

public interface MatkulService extends BaseService<Matkul> {

    List<Matkul> findByNamaMatkul(Matkul param);

}
