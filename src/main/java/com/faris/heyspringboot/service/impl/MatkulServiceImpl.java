package com.faris.heyspringboot.service.impl;

import com.faris.heyspringboot.dao.MatkulDAO;
import com.faris.heyspringboot.entity.Matkul;
import com.faris.heyspringboot.service.MatkulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatkulServiceImpl implements MatkulService {

    @Autowired
    private MatkulDAO matkulDAO;

    @Override
    public List<Matkul> findByNamaMatkul(Matkul param) {
        return matkulDAO.findByNamaMatkul(param);
    }

    @Override
    public Matkul save(Matkul param) {
        return matkulDAO.save(param);
    }

    @Override
    public Matkul update(Matkul param) {
        return matkulDAO.update(param);
    }

    @Override
    public int delete(Matkul param) {
        return matkulDAO.delete(param);
    }

    @Override
    public Matkul findById(int id) {
        return matkulDAO.findById(id);
    }

    @Override
    public List<Matkul> findAll() {
        return matkulDAO.findAll();
    }
}
