package com.faris.heyspringboot.service;

import com.faris.heyspringboot.entity.User;

import java.util.List;

public interface UserService extends BaseService<User> {

    List<User> findByUsername(User param);
}
