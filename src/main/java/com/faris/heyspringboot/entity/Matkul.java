package com.faris.heyspringboot.entity;

public class Matkul {

    protected int id_matkul;
    private String nama_matkul;
    private int sks;

    public Matkul() {

    }

    public Matkul(int id_matkul, String nama_matkul, int sks) {
        this.id_matkul = id_matkul;
        this.nama_matkul = nama_matkul;
        this.sks = sks;
    }

    public int getId_matkul() {
        return id_matkul;
    }

    public void setId_matkul(int id_matkul) {
        this.id_matkul = id_matkul;
    }

    public String getNama_matkul() {
        return nama_matkul;
    }

    public void setNama_matkul(String nama_matkul) {
        this.nama_matkul = nama_matkul;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }
}
